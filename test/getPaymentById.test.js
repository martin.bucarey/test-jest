const { getPaymentById } = require('../controllers/payment.controller');
const { getPayments } = require('../services/payment.service');

jest.mock('../services/payment.service');

describe('When call the getPaymentById fucntion with id ',() => {
    it('Should have been called once the getPayments function', () => {
        const id = 'paymentId';
        
        getPayments.mockReturnValue([{
            id: 'paymentId',
            amount: 'paymentAmmount',
        }]);
        getPaymentById(id);
        expect(getPayments).toHaveBeenCalledTimes(1);
    });

    it('Should return an array with length 1 when calling the function with an existing id',() => {
        const id = 'paymentId';
        getPayments.mockReturnValue([{
            id: 'paymentId',
            amount: 'paymentAmmount'
        },
        {
            id: 'paymentId2',
            amount: 'paymentAmmount2'
        }]);
        const expectedResult = {
            id: 'paymentId',
            amount: 'paymentAmmount'
        }
        const result = getPaymentById(id);
        expect(result).toStrictEqual(expectedResult);
    });
})