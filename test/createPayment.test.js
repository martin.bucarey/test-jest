const { createPayment } = require('../controllers/payment.controller');
const { validate } = require('../services/payment.service');
const { create } = require('../services/payment.service');

jest.mock('../services/payment.service');

beforeEach(()=> {
    validate.mockClear();
    create.mockClear();
})

describe('When call the createPayment fucntion with data', ()=>{
    it('Should have been called once the validate function',() => {
        const data = {
            id : 'paymentId',
            amount: 'paymentAmmount'
        }
        createPayment(data);
        expect(validate).toHaveBeenCalledTimes(1);
    });
    it('Should have been called once the create function',() => {
        const data = {
            id : 'paymentId',
            amount: 'paymentAmmount'
        }
        createPayment(data);
        expect(create).toHaveBeenCalledTimes(1);
    })
})  