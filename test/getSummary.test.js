const { getSummary } = require('../controllers/payment.controller');
const { getPayments } = require('../services/payment.service');

jest.mock('../services/payment.service');

beforeEach(()=> {
    getPayments.mockClear();
})

describe('When call the getSummary function ',() => {
    it('Should have been called once the validate function',() => {
        getPayments.mockReturnValue([{
            id: 'paymentId',
            amount: 'paymentAmmount',
        }]);
        getSummary();
        expect(getPayments).toHaveBeenCalledTimes(1);
    });

    it('Return total 400 and 2 count',() => {
        getPayments.mockReturnValue([{
            id: 'paymentId',
            amount: 100,
        },
        {
            id: 'paymentId2',
            amount: 300,
        }]);
        const result = getSummary();
        expect(result.total).toBe(400);
        expect(result.count).toBe(2);
    });
})