const { sortPaymentsByAmount } = require('../controllers/payment.controller');
const { getPayments } = require('../services/payment.service');

jest.mock('../services/payment.service');

beforeEach(()=> {
    getPayments.mockClear();
});

describe('When call the sortPaymentsByAmount fucntion ',() => {
    it('Should have been called once the getPayments function', () => {
        getPayments.mockReturnValue([{
            id: 'paymentId',
            amount: 'paymentAmmount',
        }]);
        sortPaymentsByAmount();
        expect(getPayments).toHaveBeenCalledTimes(1);
    });

    it('Should return an array sorted by payments in ascending order',() => {
        getPayments.mockReturnValue([{
            id: 'paymentId',
            amount: 300,
        },
        {
            id: 'paymentId2',
            amount: 100,
        },
        {
            id: 'paymentId2',
            amount: 600,
        }]);
        const result = sortPaymentsByAmount();
        expect(result[0].amount < result[1].amount).toEqual(true);
        expect(result[1].amount < result[2].amount).toEqual(true);
    });

    it('Should return an array sorted by payments in descending order',() => {
        getPayments.mockReturnValue([{
            id: 'paymentId',
            amount: 300,
        },
        {
            id: 'paymentId2',
            amount: 100,
        },
        {
            id: 'paymentId2',
            amount: 600,
        }]);
        const result = sortPaymentsByAmount(false);
        expect(result[0].amount > result[1].amount).toEqual(true);
        expect(result[1].amount > result[2].amount).toEqual(true);
    });
})