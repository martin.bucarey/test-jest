const { getPaymentGroupedByPaymentGateway } = require('../controllers/payment.controller');
const { getPayments } = require('../services/payment.service');

jest.mock('../services/payment.service');

beforeEach(()=> {
    getPayments.mockClear();
    getPayments.mockReturnValue([{
        id: 'paymentId',
        amount: 'paymentAmmount',
        payment_gateway: 'naranja'
    },
    {
        id: 'paymentId',
        amount: 'paymentAmmount',
        payment_gateway: 'naranja'
    },
    {
        id: 'paymentId',
        amount: 'paymentAmmount',
        payment_gateway: 'visa'
    }]);
})

describe('When call the getPaymentGroupedByPaymentGateway function ',() => {
    it('Should have been called once the validate function',() => {
        getPaymentGroupedByPaymentGateway();
        expect(getPayments).toHaveBeenCalledTimes(1);
    });

    it('Return 2 paymentsGroup',() => {
        const result = getPaymentGroupedByPaymentGateway();
        const paymentsGroup = Object.keys(result);
        expect(paymentsGroup.length).toBe(2);
    });

    it('Return naranja and visa paymentsGroup', () =>{
        const result = getPaymentGroupedByPaymentGateway();
        const paymentsGroup = Object.keys(result);
        expect(paymentsGroup[0]).toBe('naranja');
        expect(paymentsGroup[1]).toBe('visa');
    })
})